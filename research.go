package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"

	"github.com/charmbracelet/log"
)

const TAVILY_BASE_URL = "https://api.tavily.com"

var DEBUG = false

type SearchRequest struct {
	APIKey            string   `json:"api_key"`
	Query             string   `json:"query"`
	SearchDepth       string   `json:"search_depth"`
	IncludeAnswer     bool     `json:"include_answer"`
	IncludeImages     bool     `json:"include_images"`
	IncludeRawContent bool     `json:"include_raw_content"`
	MaxResults        int      `json:"max_results"`
	IncludeDomains    []string `json:"include_domains"`
	ExcludeDomains    []string `json:"exclude_domains"`
}

type SearchResult struct {
	Title      string  `json:"title"`
	URL        string  `json:"url"`
	Content    string  `json:"content"`
	RawContent string  `json:"raw_content"`
	Score      float64 `json:"score"`
}

type SearchResponse struct {
	Answer            string         `json:"answer"`
	Query             string         `json:"query"`
	ResponseTime      float64        `json:"response_time"`
	FollowUpQuestions []string       `json:"follow_up_questions"`
	Images            []string       `json:"images"`
	Results           []SearchResult `json:"results"`
}

func postSearchRequest(query string) (*SearchResponse, error) {
	apiKey := os.Getenv("TAVILY_API_KEY")
	DEBUG = os.Getenv("DEBUG") == "true"
	fullURL := TAVILY_BASE_URL + "/search"

	searchReq := SearchRequest{
		APIKey:            apiKey,
		Query:             query,
		SearchDepth:       "basic",
		IncludeAnswer:     false,
		IncludeImages:     false,
		IncludeRawContent: false,
		MaxResults:        5,
		IncludeDomains:    []string{},
		ExcludeDomains:    []string{},
	}

	if DEBUG {
		log.Info(fmt.Sprintf("\nsearchRequest: %s", prettyPrint(searchReq)))
	}

	requestBody, err := json.Marshal(searchReq)
	chkmsg(err, "Failed to marshal search request...")

	resp, err := http.Post(fullURL, "application/json", bytes.NewBuffer(requestBody))
	chkmsg(err, "Failed to post search request...")
	defer resp.Body.Close()

	dump, err := httputil.DumpResponse(resp, true)
	chkmsg(err, "Failed to dump response...")

	if DEBUG {
		log.Info(fmt.Sprintf("\nRaw Response: %s", dump))
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("search request failed: status code %d", resp.StatusCode)
	}

	if DEBUG {
		log.Info(fmt.Sprintf("\nRaw Response Body: %s", resp.Body))
	}

	var searchResp SearchResponse
	chkmsg(json.NewDecoder(resp.Body).Decode(&searchResp), "Failed to decode search response...")

	if DEBUG {
		log.Info(fmt.Sprintf("\nsearchResponse: %s", prettyPrint(searchResp)))
	}

	for _, result := range searchResp.Results {
		fmt.Println("------------------------")
		fmt.Println(result.Title)
		fmt.Println(result.Content)
		fmt.Println("------------------------")
	}

	return &searchResp, nil
}

func prettyPrint(v interface{}) string {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		return fmt.Sprintf("error: %v", err)
	}
	return string(b)
}

func main() {
	// TODO: Problem, this works but doesn't. Not real results. Might have to use the google official api
	_, err := postSearchRequest("What is GPT?")
	chkmsg(err, "Posting search request failed...")
}

///////////////////////////
// Error handling helpers
///////////////////////////

func chkmsg(err error, message string) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func chk(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
